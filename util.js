const polyfill = require("./polyfill");
const exclude = [":", "/", "?", "#", "[", "]", "@", "!", "$", "&", "'", "\"", "^", "(", ")", "*", "+", ",", ";", "="];
const excludeRegexp = new RegExp(`[\\${exclude.join("\\")}]`, "g");

function tax(total, taxRate) {
  return polyfill.trunc(total - total / (1 + taxRate));
};

function pricify(p) {
  return (Number(p) / 100).toFixed(2).replace(".", ",") + " €";
};

function quantify(n) {
  var arr = [];
  for (var i = 1; i <= n; i++) {
    arr.push(i)
  }
  return arr;
};


function uniformDelims(text) {
  const delims = text.replace(excludeRegexp, "_");
  return delims.replace(/[-_\s]+/g, "-");
};


module.exports = { tax, pricify, quantify, uniformDelims };