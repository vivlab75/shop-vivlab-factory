function trunc(v) {
  return v < 0 ? Math.ceil(v) : Math.floor(v);
}


function find(list, fn) {
  for (var i = 0; i < list.length; i++) {
    var valid = fn.call(null, list[i]);
    if (valid === true) {
      return list[i];
    }
  }
  return undefined;
}


function assign() {
  var obj = {};
  for (var i = 0; i < arguments.length; i++) {
    var keys = Object.keys(arguments[i]);
    for (var j = 0; j < keys.length; j++) {
      obj[keys[j]] = arguments[i][keys[j]];
    }
  }
  return obj;
}

module.exports = {
  trunc: trunc,
  find: find,
  assign: assign
}