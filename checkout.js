const polyfill = require("./polyfill");

function articles(products, skus) {
  return skus.reduce(function (a, v) {
    var item = polyfill.find(products, function (p) {
      return p.sku === v.sku && (p.inventory_quantity >= v.quantity || p.inventory_type === "infinite");
    });
    return item ? a.concat(polyfill.assign(item, v)) : a;
  }, []);
};


function shipment(shippings, sku, threshold) {
  var shippable = shippings.reduce(function (a, v) {
    if (v.free_shipping_at !== null && threshold >= v.free_shipping_at) {
      return a.concat(polyfill.assign(v, { price: 0 }));
    }
    else {
      return a.concat(v);
    }
  }, []);
  return polyfill.find(shippable, function (e) {
    return e.sku === sku
  });
};

function redeem(coupon, subtotal) {
  if (coupon == null) return false;
  var assertion = coupon.valid === true && coupon.active === true && subtotal >= coupon.valid_coupon_at;
  if (assertion === false) return false;
  var discount = coupon.amount_off || (subtotal * (coupon.percent_off / 100));
  return {
    discount: polyfill.trunc(Math.min(discount, subtotal) * 100) / 100,
    amount_off: coupon.amount_off,
    percent_off: coupon.percent_off
  };
}

function checkout(reference, elements) {
  var _articles = articles(reference.products, elements.carts);
  var subtotal = _articles.reduce(function (a, v) {
    return a + v.price * v.quantity;
  }, 0);

  var _redeem = redeem(elements.coupon, subtotal);
  var discount = _redeem ? _redeem.discount : 0;

  var _shipment = shipment(reference.shippings, elements.shipping_sku, subtotal - discount);
  var shipping = _shipment ? _shipment.price : 0;

  return {
    subtotal: subtotal,
    discount: discount,
    shipping: shipping,
    total: subtotal - discount + shipping
  };
};

module.exports = {
  checkout: checkout,
  redeem: redeem,
  shipment: shipment,
  articles: articles
};